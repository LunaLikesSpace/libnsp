/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file main.cpp
 * @author Robin Lilja
 * @date 20 Nov 2018
 */

#include <stdio.h>
#include <stdint.h>

#include "gtest/gtest.h"
//#include "gmock/gmock.h"

#include "libnsp/nsp.h"
#include "libnsp/nsp_common.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
