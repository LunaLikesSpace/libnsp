/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file nsp.cpp
 * @author Robin Lilja
 * @date 15 Nov 2018
 */

#include "libnsp/nsp.h"

namespace NSP {

/*
 * Calculate 16-bit CRC-CCITT. Note that the CRC can differ between manufacturers.
 * @param buf data buffer.
 * @param buflen length of data buffer.
 * @return CRC value
 */
static uint16_t crc16(const unsigned char * buf, uint16_t buflen);

uint16_t encode(const nsp_header_t * hdr, unsigned char * frm, const unsigned char * dat, uint16_t datlen) {

  uint16_t msglen = NSP_MIN_LEN + datlen;

  unsigned char msg[msglen];

  uint16_t i = 0;

  // Add header
  msg[i++] = hdr->m_dst;
  msg[i++] = hdr->m_src;
  msg[i++] = hdr->m_mcf;

  // Add data field
  memcpy(msg+i, dat, datlen);
  i += datlen;

  // Calculate CRC
  uint16_t crc = crc16(msg, i);

  // Add CRC (little endian)
  msg[i++] = crc & 0xFF;  // Lower
  msg[i++] = crc >> 8;    // Higher

  // Encode message with SLIP
  return SLIP::encode(frm, msg, msglen);
}

uint16_t decodeNoSLIP(nsp_header_t * hdr, unsigned char * dat, const unsigned char * frm, uint16_t frmlen, nsp_error_t * err) {
  // Assume no error by default
  *err = NSP_ERROR::NO_ERROR;

  // Assert message length meets minium length requirement
  if (frmlen < NSP_MIN_LEN) { *err = NSP_ERROR::MIN_LEN_NOK; return 0;}

  uint16_t i = 0;

  // Get header
  hdr->m_dst = frm[i++];
  hdr->m_src = frm[i++];
  hdr->m_mcf = frm[i++];

  // Set data length
  uint16_t datlen = frmlen-NSP_MIN_LEN;

  // Get data field
  memcpy(dat, frm+i, datlen);

  // Assert CRC is correct
  if (NSP_CRC_OK != crc16(frm, frmlen)) { *err = NSP_ERROR::CRC_NOK; }

  return datlen;
}

uint16_t decode(nsp_header_t * hdr, unsigned char * dat, const unsigned char * frm, uint16_t frmlen, nsp_error_t * err) {

  // Message will have less or equal length as the frame (minus leading and trailing FEND character)
  unsigned char msg[frmlen];

  // Decode frame with SLIP
  uint16_t msglen = SLIP::decode(msg, frm, frmlen);

  return decodeNoSLIP(hdr, dat, msg, msglen, err);
}

static uint16_t crc16(const unsigned char * buf, uint16_t buflen) {

  // The following code, found on public domain, is based on the work of Henry Spencer.

  // Set initial CRC
  unsigned short crc = NSP_CRC_INIT;

  // Iterate byte buffer
  while (buflen-- > 0) {

    // Get byte from buffer and increment buffer pointer
    unsigned char ch = *buf++;

    // Iterate bits of current byte
    for (int i = 0; i < 8; i++) {

      crc = (crc >> 1) ^ ( ((ch ^ crc) & 0x01) ? NSP_CRC_POLY : 0 );
      ch >>= 1;
    }
  }

  return crc;
}

uint16_t getFrame(unsigned char * frm, const unsigned char * fifo, uint16_t fifonum, bool sync) {

  uint16_t offset = 0;

  if (sync) {

    // Flush spurious bytes preceding first occuring FEND character
    for (uint16_t i = 0; i < fifonum; i++) {

      if (fifo[i] == SLIP_CHAR::FEND) { offset = i; break; }
    }

    // Flush arbitrary amount of FEND characters until true start of frame FEND character
    for (uint16_t i = offset; i < fifonum-offset-1; i++) {

      if (fifo[i] == SLIP_CHAR::FEND && fifo[i+1] != SLIP_CHAR::FEND) { offset = i; break; }
    }

  }

  return SLIP::getFrame(frm, fifo+offset, fifonum-offset);

}

}
